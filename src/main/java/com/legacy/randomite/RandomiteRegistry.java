package com.legacy.randomite;

import net.minecraft.world.item.BlockItem;
import net.minecraft.world.item.CreativeModeTabs;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.neoforged.neoforge.event.BuildCreativeModeTabContentsEvent;
import net.neoforged.neoforge.registries.DeferredHolder;
import net.neoforged.neoforge.registries.DeferredItem;
import net.neoforged.neoforge.registries.DeferredRegister;

public class RandomiteRegistry
{
	public static interface BlockReg
	{
		DeferredRegister.Blocks BLOCKS = DeferredRegister.createBlocks(RandomiteMod.MODID);

		DeferredHolder<Block, Block> RANDOMITE_ORE = BLOCKS.registerSimpleBlock("randomite_ore", Block.Properties.ofFullCopy(Blocks.DIAMOND_ORE));
		DeferredHolder<Block, Block> DEEPSLATE_RANDOMITE_ORE = BLOCKS.registerSimpleBlock("deepslate_randomite_ore", Block.Properties.ofFullCopy(Blocks.DEEPSLATE_DIAMOND_ORE));
	}

	public static interface ItemReg
	{
		DeferredRegister.Items ITEMS = DeferredRegister.createItems(RandomiteMod.MODID);

		DeferredItem<BlockItem> RANDOMITE_ORE = ITEMS.registerSimpleBlockItem(BlockReg.RANDOMITE_ORE);
		DeferredItem<BlockItem> DEEPSLATE_RANDOMITE_ORE = ITEMS.registerSimpleBlockItem(BlockReg.DEEPSLATE_RANDOMITE_ORE);
	}

	public static void onRegisterCreativeTabs(BuildCreativeModeTabContentsEvent event)
	{
		if (event.getTabKey() == CreativeModeTabs.NATURAL_BLOCKS)
		{
			event.accept(ItemReg.RANDOMITE_ORE);
			event.accept(ItemReg.DEEPSLATE_RANDOMITE_ORE);
		}
	}
}