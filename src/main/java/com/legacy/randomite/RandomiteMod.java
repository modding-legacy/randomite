package com.legacy.randomite;

import net.minecraft.resources.ResourceLocation;
import net.neoforged.bus.api.IEventBus;
import net.neoforged.fml.common.Mod;

@Mod(RandomiteMod.MODID)
public class RandomiteMod
{
	public static final String NAME = "Randomite Classic";
	public static final String MODID = "randomite";

	public RandomiteMod(IEventBus modBus)
	{
		RandomiteRegistry.BlockReg.BLOCKS.register(modBus);
		RandomiteRegistry.ItemReg.ITEMS.register(modBus);
		modBus.addListener(RandomiteRegistry::onRegisterCreativeTabs);
	}

	public static ResourceLocation locate(String name)
	{
		return ResourceLocation.fromNamespaceAndPath(RandomiteMod.MODID, name);
	}

	public static String find(String name)
	{
		return RandomiteMod.MODID + ":" + name;
	}

}