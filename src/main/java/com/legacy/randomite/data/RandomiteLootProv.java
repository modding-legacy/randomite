package com.legacy.randomite.data;

import java.util.List;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Stream;

import com.legacy.randomite.RandomiteMod;

import net.minecraft.core.HolderLookup;
import net.minecraft.core.WritableRegistry;
import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.core.registries.Registries;
import net.minecraft.data.DataGenerator;
import net.minecraft.data.loot.BlockLootSubProvider;
import net.minecraft.data.loot.LootTableProvider;
import net.minecraft.util.ProblemReporter;
import net.minecraft.world.flag.FeatureFlags;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.enchantment.Enchantment;
import net.minecraft.world.item.enchantment.Enchantments;
import net.minecraft.world.level.ItemLike;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.storage.loot.LootPool;
import net.minecraft.world.level.storage.loot.LootTable;
import net.minecraft.world.level.storage.loot.ValidationContext;
import net.minecraft.world.level.storage.loot.entries.LootItem;
import net.minecraft.world.level.storage.loot.entries.LootPoolEntryContainer;
import net.minecraft.world.level.storage.loot.functions.ApplyBonusCount;
import net.minecraft.world.level.storage.loot.functions.SetItemCountFunction;
import net.minecraft.world.level.storage.loot.parameters.LootContextParamSets;
import net.minecraft.world.level.storage.loot.providers.number.ConstantValue;
import net.minecraft.world.level.storage.loot.providers.number.UniformGenerator;
import net.neoforged.neoforge.common.data.ExistingFileHelper;

public class RandomiteLootProv extends LootTableProvider
{
	public RandomiteLootProv(DataGenerator gen, ExistingFileHelper fileHelper, CompletableFuture<HolderLookup.Provider> lookup)
	{
		super(gen.getPackOutput(), Set.of(), List.of(new LootTableProvider.SubProviderEntry(RandomiteBlockLoot::new, LootContextParamSets.BLOCK)), lookup);
	}

	@Override
	protected void validate(WritableRegistry<LootTable> writableregistry, ValidationContext validationcontext, ProblemReporter.Collector problemreporter$collector)
	{
		writableregistry.listElements().forEach(lootTable -> lootTable.value().validate(validationcontext.setContextKeySet(lootTable.value().getParamSet()).enterElement("{" + lootTable.key().location() + "}", lootTable.key())));
	}

	private static class RandomiteBlockLoot extends BlockLootSubProvider
	{
		protected RandomiteBlockLoot(HolderLookup.Provider lookup)
		{
			super(Set.of(), FeatureFlags.REGISTRY.allFlags(), lookup);
		}

		@Override
		protected void generate()
		{
			HolderLookup.RegistryLookup<Enchantment> ench = this.registries.lookupOrThrow(Registries.ENCHANTMENT);
			var oreBonus = ApplyBonusCount.addOreBonusCount(ench.getOrThrow(Enchantments.FORTUNE));

			// @formatter:off
	        LootPool.Builder randomiteOre = poolOf(List.of(
					basicEntry(Items.COAL).setWeight(25),
					basicEntry(Items.LAPIS_LAZULI, 4, 9).setWeight(27),
					basicEntry(Items.EGG).setWeight(20),
					basicEntry(Items.REDSTONE, 4, 5).setWeight(10),
					basicEntry(Items.QUARTZ, 1, 2).setWeight(10),
					basicEntry(Items.DIAMOND).setWeight(3),
					basicEntry(Items.EMERALD).setWeight(5),
					basicEntry(Items.RAW_COPPER, 2, 3).setWeight(25),
					basicEntry(Items.SLIME_BALL, 1, 2).setWeight(30),
					basicEntry(Items.RAW_GOLD).setWeight(10),
					basicEntry(Items.RAW_IRON).setWeight(13)
				)).apply(oreBonus).setRolls(ConstantValue.exactly(1.0F));


			blocks().forEach(block -> this.add(block, (b) -> LootTable.lootTable()
					.withPool(LootPool.lootPool().setRolls(ConstantValue.exactly(1.0F)).add(LootItem.lootTableItem(b).when(this.hasSilkTouch())))
					.withPool(this.applyExplosionDecay(b, randomiteOre).when(this.doesNotHaveSilkTouch()))));
			// @formatter:on
		}

		@Override
		protected Iterable<Block> getKnownBlocks()
		{
			return blocks()::iterator;
		}

		private Stream<Block> blocks()
		{
			return BuiltInRegistries.BLOCK.stream().filter(b -> BuiltInRegistries.BLOCK.getKey(b).getNamespace().equals(RandomiteMod.MODID));
		}

		private LootPool.Builder poolOf(List<LootPoolEntryContainer.Builder<?>> lootEntries)
		{
			LootPool.Builder pool = LootPool.lootPool();
			lootEntries.forEach(entry -> pool.add(entry));
			return pool;
		}

		private LootItem.Builder<?> basicEntry(ItemLike item, int min, int max)
		{
			return basicEntry(item).apply(SetItemCountFunction.setCount(UniformGenerator.between(min, max)));
		}

		private LootItem.Builder<?> basicEntry(ItemLike item)
		{
			return LootItem.lootTableItem(item);
		}
	}
}
