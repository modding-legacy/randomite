package com.legacy.randomite;

import java.util.List;

import net.minecraft.core.HolderSet;
import net.minecraft.core.registries.Registries;
import net.minecraft.data.worldgen.BootstrapContext;
import net.minecraft.data.worldgen.features.FeatureUtils;
import net.minecraft.data.worldgen.placement.PlacementUtils;
import net.minecraft.data.worldgen.placement.TreePlacements;
import net.minecraft.resources.ResourceKey;
import net.minecraft.tags.BlockTags;
import net.minecraft.world.level.levelgen.GenerationStep;
import net.minecraft.world.level.levelgen.VerticalAnchor;
import net.minecraft.world.level.levelgen.feature.ConfiguredFeature;
import net.minecraft.world.level.levelgen.feature.Feature;
import net.minecraft.world.level.levelgen.feature.configurations.OreConfiguration;
import net.minecraft.world.level.levelgen.placement.BiomeFilter;
import net.minecraft.world.level.levelgen.placement.CountPlacement;
import net.minecraft.world.level.levelgen.placement.HeightRangePlacement;
import net.minecraft.world.level.levelgen.placement.InSquarePlacement;
import net.minecraft.world.level.levelgen.placement.PlacedFeature;
import net.minecraft.world.level.levelgen.structure.templatesystem.TagMatchTest;
import net.neoforged.neoforge.common.Tags;
import net.neoforged.neoforge.common.world.BiomeModifier;
import net.neoforged.neoforge.common.world.BiomeModifiers;
import net.neoforged.neoforge.registries.NeoForgeRegistries;

public class RandomiteFeatures
{
	public static final ResourceKey<ConfiguredFeature<?, ?>> CONFIGURED_RANDOMITE_ORE = ResourceKey.create(Registries.CONFIGURED_FEATURE, RandomiteMod.locate("randomite_ore"));
	public static final ResourceKey<PlacedFeature> PLACED_RANDOMITE_ORE = ResourceKey.create(Registries.PLACED_FEATURE, RandomiteMod.locate("randomite_ore"));
	public static final ResourceKey<BiomeModifier> OVERWORLD_RANDOMITE_MODIFIER = ResourceKey.create(NeoForgeRegistries.Keys.BIOME_MODIFIERS, RandomiteMod.locate("add_overworld_randomite_ore"));

	public static void configuredBootstrap(BootstrapContext<ConfiguredFeature<?, ?>> bootstrap)
	{
		var targetList = List.of(OreConfiguration.target(new TagMatchTest(BlockTags.STONE_ORE_REPLACEABLES), RandomiteRegistry.BlockReg.RANDOMITE_ORE.get().defaultBlockState()), OreConfiguration.target(new TagMatchTest(BlockTags.DEEPSLATE_ORE_REPLACEABLES), RandomiteRegistry.BlockReg.DEEPSLATE_RANDOMITE_ORE.get().defaultBlockState()));
		FeatureUtils.register(bootstrap, CONFIGURED_RANDOMITE_ORE, Feature.ORE, new OreConfiguration(targetList, 4));
	}

	public static void placedBootstrap(BootstrapContext<PlacedFeature> bootstrap)
	{
		bootstrap.lookup(Registries.PLACED_FEATURE).get(TreePlacements.CRIMSON_FUNGI);
		PlacementUtils.register(bootstrap, PLACED_RANDOMITE_ORE, bootstrap.lookup(Registries.CONFIGURED_FEATURE).getOrThrow(CONFIGURED_RANDOMITE_ORE), List.of(CountPlacement.of(2), InSquarePlacement.spread(), HeightRangePlacement.triangle(VerticalAnchor.absolute(-32), VerticalAnchor.absolute(32)), BiomeFilter.biome()));
	}
	
	public static void modifierBootstrap(BootstrapContext<BiomeModifier> bootstrap)
	{
		var biome = bootstrap.lookup(Registries.BIOME);
		var feature = bootstrap.lookup(Registries.PLACED_FEATURE);

		bootstrap.register(OVERWORLD_RANDOMITE_MODIFIER, new BiomeModifiers.AddFeaturesBiomeModifier(biome.getOrThrow(Tags.Biomes.IS_OVERWORLD), HolderSet.direct(feature.getOrThrow(PLACED_RANDOMITE_ORE)), GenerationStep.Decoration.UNDERGROUND_ORES));
	}
}